const storeFake = (state) => {
	return {
		default: jest.fn(),
		subscribe: jest.fn(),
		dispatch: jest.fn(),
		getState: () => state
	};
};

module.exports = {
	storeFake: storeFake
};