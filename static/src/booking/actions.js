var types = require('./actionTypes.js');

const updateAirport = (airport, end) => {
	return {
		type: types.AIRPORT_UPDATED,
		airport: airport,
		end: end
	};
};

const updateOutboundDate = (date) => {
	return {
		type: types.OUTBOUND_DATE_UPDATED,
		date: date
	};
};

const updateReturnDate = (date) => {
	return {
		type: types.RETURN_DATE_UPDATED,
		date: date
	};
};

const updateDates = (out, ret) => {
	return {
		type: types.DATES_UPDATED,
		out: out,
		ret: ret
	};
};

const updateNumberPassengers = (passengerType, number) => {
	return {
		type: types.NUMBER_PASSENGERS_UPDATED,
		passengerType: passengerType,
		number: number
	};
};

const updateFlightType = (isRoundTrip) => {
	return {
		type: types.FLIGHT_TYPE_UPDATED,
		isRoundTrip: isRoundTrip
	};
};

const addNewSearch = (newSearch) => {
	return {
		type: types.NEW_SEARCH_ADDED,
		newSearch: newSearch
	}
};

const doInvalidSearch = (invalidParam) => {
	return {
		type: types.INVALID_SEARCH_DONE,
		invalidParam: invalidParam
	}
};

module.exports = {
	updateAirport: updateAirport,
	updateOutboundDate: updateOutboundDate,
	updateReturnDate: updateReturnDate,
	updateDates: updateDates,
	updateNumberPassengers: updateNumberPassengers,
	updateFlightType: updateFlightType,
	addNewSearch: addNewSearch,
	doInvalidSearch: doInvalidSearch
};