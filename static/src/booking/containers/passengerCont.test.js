const React = require('react');
const {shallow, mount} = require('enzyme');
const {Provider} = require('react-redux');
const {PassengerCont} = require('./passengerCont.js');
const {PassengerComp} = require('../components/passengerComp.js');
const {storeFake} = require('../../../test/utils.js');
const {initialState} = require('../reducer.js');

describe('PassengerCont', () => {
	let wrapper;
	let component;
	let container;

	beforeEach(() => {
		jest.resetAllMocks();

		const store = storeFake(initialState);

		wrapper =  mount(
			<Provider store={store}>
				<PassengerCont />
			</Provider>
		);

		container = wrapper.find(PassengerCont);
		component = container.find(PassengerComp)
	});

	it('should render both the container and the component ', () => {
	    expect(container.length).toBeTruthy();
	    expect(component.length).toBeTruthy();
	});

	it('should map to props', () => {
		const expectedPropKeys = [
			'value',
			'min',
			'max',
			'label',
			'info',
			'increment',
			'decrement'
		];

		expect(Object.keys(component.props())).toEqual(expect.arrayContaining(expectedPropKeys));
	});
});
