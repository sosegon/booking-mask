const React = require('react');
const {shallow, mount} = require('enzyme');
const {Provider} = require('react-redux');
const {PassengersModCont} = require('./passengersModCont.js');
const {PassengersModComp} = require('../components/passengersModComp.js');
const {storeFake} = require('../../../test/utils.js');
const {initialState} = require('../reducer.js');

describe('PassengersModCont', () => {
	let wrapper;
	let component;
	let container;

	beforeEach(() => {
		jest.resetAllMocks();

		const store = storeFake(initialState);

		wrapper =  mount(
			<Provider store={store}>
				<PassengersModCont />
			</Provider>
		);

		container = wrapper.find(PassengersModCont);
		component = container.find(PassengersModComp)
	});

	it('should render both the container and the component ', () => {
	    expect(container.length).toBeTruthy();
	    expect(component.length).toBeTruthy();
	});

	it('should map to props', () => {
		const expectedPropKeys = [];

		expect(Object.keys(component.props())).toEqual(expect.arrayContaining(expectedPropKeys));
	});
});
