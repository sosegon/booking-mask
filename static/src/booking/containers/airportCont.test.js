const React = require('react');
const {shallow, mount} = require('enzyme');
const {Provider} = require('react-redux');
const {AirportCont} = require('./airportCont.js');
const {AirportComp} = require('../components/airportComp.js');
const {initialState} = require('../reducer.js');
const {storeFake} = require('../../../test/utils.js');

describe('AirportCont', () => {
	let wrapper;
	let component;
	let container;

	beforeEach(() => {
		jest.resetAllMocks();

		const store = storeFake(initialState);

		wrapper =  mount(
			<Provider store={store}>
				<AirportCont />
			</Provider>
		);

		container = wrapper.find(AirportCont);
		component = container.find(AirportComp)
	});

	it('should render both the container and the component ', () => {
	    expect(container.length).toBeTruthy();
	    expect(component.length).toBeTruthy();
	});

	it('should map to props', () => {
		const expectedPropKeys = [
			'onSuggestionSelected'
		];

		expect(Object.keys(component.props())).toEqual(expect.arrayContaining(expectedPropKeys));
	});
});
