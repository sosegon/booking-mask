require('react-dates/initialize');
const React = require('react');
const {shallow, mount} = require('enzyme');
const {Provider} = require('react-redux');
const {DateCont} = require('./dateCont.js');
const {DateComp} = require('../components/dateComp.js');
const {storeFake} = require('../../../test/utils.js');
const {initialState} = require('../reducer.js');

describe('DateCont', () => {
	let wrapper;
	let component;
	let container;

	beforeEach(() => {
		jest.resetAllMocks();

		const store = storeFake(initialState);

		wrapper =  mount(
			<Provider store={store}>
				<DateCont />
			</Provider>
		);

		container = wrapper.find(DateCont);
		component = container.find(DateComp)
	});

	it('should render both the container and the component ', () => {
	    expect(container.length).toBeTruthy();
	    expect(component.length).toBeTruthy();
	});

	it('should map to props', () => {
		const expectedPropKeys = [
			'startDate',
			'endDate',
			'isRoundTrip',
			'onDatesChange',
			'onOutboundDateChange',
			'onToggleFlights'
		];

		expect(Object.keys(component.props())).toEqual(expect.arrayContaining(expectedPropKeys));
	});
});
