const React = require('react');
const {connect} = require('react-redux');
const actions = require('../actions.js');
const {AirportComp} = require('../components/airportComp.js');

const mapStateToProps = (state) => {
	return {
	};
};

const mapDispatchToProps = (dispatch, ownProps) => {
	return {
		onSuggestionSelected: (event, {suggestionValue}) => {
			dispatch(
				actions.updateAirport(suggestionValue, ownProps.end)
			);
		}
	};
};

const AirportCont = connect(mapStateToProps, mapDispatchToProps)(AirportComp);

module.exports = {
	AirportCont: AirportCont
};