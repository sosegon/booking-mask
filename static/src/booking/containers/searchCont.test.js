const React = require('react');
const {shallow, mount} = require('enzyme');
const {Provider} = require('react-redux');
const {SearchCont} = require('./searchCont.js');
const {SearchComp} = require('../components/searchComp.js');
const {storeFake} = require('../../../test/utils.js');
const {initialState} = require('../reducer.js');

describe('SearchCont', () => {
	let wrapper;
	let component;
	let container;

	beforeEach(() => {
		jest.resetAllMocks();

		const store = storeFake(initialState);

		wrapper =  mount(
			<Provider store={store}>
				<SearchCont />
			</Provider>
		);

		container = wrapper.find(SearchCont);
		component = container.find(SearchComp)
	});

	it('should render both the container and the component ', () => {
	    expect(container.length).toBeTruthy();
	    expect(component.length).toBeTruthy();
	});

	it('should map to props', () => {
		const expectedPropKeys = [
			'params',
			'onSearch'
		];

		expect(Object.keys(component.props())).toEqual(expect.arrayContaining(expectedPropKeys));
	});
});
