const React = require('react');
const {shallow, mount} = require('enzyme');
const {Provider} = require('react-redux');
const {AirportsModCont} = require('./airportsModCont.js');
const {AirportsModComp} = require('../components/airportsModComp.js');
const {storeFake} = require('../../../test/utils.js');
const {initialState} = require('../reducer.js');

describe('AirportsModCont', () => {
	let wrapper;
	let component;
	let container;

	beforeEach(() => {
		jest.resetAllMocks();

		const store = storeFake(initialState);

		wrapper =  mount(
			<Provider store={store}>
				<AirportsModCont />
			</Provider>
		);

		container = wrapper.find(AirportsModCont);
		component = container.find(AirportsModComp)
	});

	it('should render both the container and the component ', () => {
	    expect(container.length).toBeTruthy();
	    expect(component.length).toBeTruthy();
	});

	it('should map to props', () => {
		const expectedPropKeys = [];

		expect(Object.keys(component.props())).toEqual(expect.arrayContaining(expectedPropKeys));
	});
});
