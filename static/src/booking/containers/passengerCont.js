const {connect} = require('react-redux');
const {PASSENGER_TYPES} = require('../reducer.js');
const {PassengerComp} = require('../components/passengerComp.js');
const actions = require('../actions.js');

const getPassengers = (state, passengerType) => {
	switch(passengerType) {
		case PASSENGER_TYPES.adults:
			return state.current.passengers.adults;
		case PASSENGER_TYPES.children:
			return state.current.passengers.children;
		case PASSENGER_TYPES.infants:
			return state.current.passengers.infants;
		default:
			return state.current.passengers.adults;
	}
};

const getLabel = (passengerType) => {
	switch(passengerType) {
		case PASSENGER_TYPES.adults:
			return "Adults";
		case PASSENGER_TYPES.children:
			return "Children";
		case PASSENGER_TYPES.infants:
			return "Infants";
		default:
			return "Adults";
	}
};

const getMin = (passengerType) => {
	return passengerType === PASSENGER_TYPES.adults ? 1 : 0;
};

const verifyIncrement = (value, max, passengerType) => {
	return (dispatch, getState) => {
		if(value < max) {
			dispatch(actions.updateNumberPassengers(passengerType, value+1));
		}
	};
};

const verifyDecrement = (value, min, passengerType) => {
	return (dispatch, getState) => {
		if(value > min) {
			dispatch(actions.updateNumberPassengers(passengerType, value-1));
		}
	};
};

const mapStateToProps = (state, ownProps) => {
	return {
		value: getPassengers(state, ownProps.passengerType),
		min: getMin(ownProps.passengerType),
		max: 20,
		label: getLabel(ownProps.passengerType),
		info: ownProps.info
	};
};

const mapDispatchToProps = (dispatch, ownProps) => {
	return {
		onIncrement: (value, max) => {
			dispatch(verifyIncrement(value, max, ownProps.passengerType));
		},
		onDecrement: (value, min) => {
			dispatch(verifyDecrement(value, min, ownProps.passengerType));
		}
	};
};

const mergeProps = (propsFromState, propsFromDispatch) => {
	return {
		...propsFromState,
		increment: () => propsFromDispatch.onIncrement(propsFromState.value, propsFromState.max),
		decrement: () => propsFromDispatch.onDecrement(propsFromState.value, propsFromState.min)
	};
};

const PassengerCont = connect(mapStateToProps, mapDispatchToProps, mergeProps)(PassengerComp);

module.exports = {
	PassengerCont: PassengerCont
};