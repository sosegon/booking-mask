const {connect} = require('react-redux');
const buildUrl = require('build-url');
const {SearchComp} = require('../components/searchComp.js');
const actions = require('../actions.js');
const {INVALID_SEARCH_PARAMS} = require('../reducer.js');

const validateSearch = (params) => {
	if(params.origin === '') {
		return INVALID_SEARCH_PARAMS.origin;
	}
	if(params.destination === '') {
		return INVALID_SEARCH_PARAMS.destination;
	}
	if(params.outboundDate === '') {
		return INVALID_SEARCH_PARAMS.outboundDate;
	}
	if(params.returnDate === '') {
		return INVALID_SEARCH_PARAMS.returnDate;
	}
	return INVALID_SEARCH_PARAMS.none;
};

const createUrl = (params) => {
	return buildUrl(
		'https://www.swiss.com/us/en',
		{
			path: 'Book',
			queryParams: {
				outbound: params.outboundDate,
				return: params.returnDate,
				origin: params.origin,
				destination: params.destination,
				adults: params.passengers.adults,
				children: params.passengers.children,
				infants: params.passengers.infants
			}
		}
	);
}

const trySearch = () => {
	return (dispatch, getState) => {
		const params = getState().current;
		const invalidParam = validateSearch(params);
		if(invalidParam === INVALID_SEARCH_PARAMS.none) {
			dispatch(actions.addNewSearch(params));
			console.log(createUrl(params));
		} else {
			dispatch(actions.doInvalidSearch(invalidParam));
			console.log(invalidParam);
		}
	}
}

const mapStateToProps = (state) => {
	return {
		params: state.current
	};
};

const mapDispatchToProps = (dispatch) => {
	return {
		onSearch: () => {
			dispatch(trySearch())
		}
	};
};

const SearchCont = connect(mapStateToProps, mapDispatchToProps)(SearchComp);

module.exports = {
	SearchCont: SearchCont
};
