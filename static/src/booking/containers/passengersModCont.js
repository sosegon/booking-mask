const {connect} = require('react-redux');
const {PassengersModComp} = require('../components/passengersModComp.js');

const mapStateToProps = (state) => {
	return {
		adults: state.current.passengers.adults,
		children: state.current.passengers.children,
		infants: state.current.passengers.infants
	}
};

const PassengersModCont = connect(mapStateToProps)(PassengersModComp);

module.exports = {
	PassengersModCont: PassengersModCont
};