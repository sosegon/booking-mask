const {connect} = require('react-redux');
const {AirportsModComp} = require('../components/airportsModComp.js');

const mapStateToProps = (state) => {
	return {};
};

const AirportsModCont = connect(mapStateToProps)(AirportsModComp);

module.exports = {
	AirportsModCont: AirportsModCont
};