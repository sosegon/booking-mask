const React = require('react');
const {connect} = require('react-redux');
const moment = require('moment');
const actions = require('../actions.js');
const {DateComp} = require('../components/dateComp.js');

function formatDate(date) {
  var day = date.getDate();
  var monthIndex = date.getMonth() + 1;
  var year = date.getFullYear();

  return year + '-' + monthIndex + '-' + day;
}

function getMoment(stringDate) {
	return stringDate === '' ? moment() : moment(stringDate);
}

function getAction(startDate, endDate) {
	 if(startDate !== null && endDate !== null) {
	 	return actions.updateDates(
	 		formatDate(startDate.toDate()),
			formatDate(endDate.toDate())
	 	);
	 } else if(startDate === null && endDate !== null) {
	 	return actions.updateReturnDate(formatDate(endDate.toDate()));
	 } else if(startDate !== null && endDate === null) {
	 	return actions.updateOutboundDate(formatDate(startDate.toDate()));
	 } else {
	 	// TODO: This is not expected to happen
	 }
}

const checkFlightType = () => {
	return (dispatch, getState) => {
		const isRoundTrip = getState().current.isRoundTrip;
		dispatch(actions.updateFlightType(!isRoundTrip));
	};
};

const mapStateToProps = (state) => {
	return {
		startDate: getMoment(state.current.outboundDate),
		endDate: getMoment(state.current.returnDate),
		isRoundTrip: state.current.isRoundTrip
	};
};

const mapDispatchToProps = (dispatch) => {
	return {
		onDatesChange: (range) => {
			const {startDate, endDate} = range;
			dispatch(
				getAction(startDate, endDate)
			);
		},
		onOutboundDateChange: (date) => {
			dispatch(
				actions.updateOutboundDate(formatDate(date.toDate()))
			)
		},
		onToggleFlights: (toggle) => {
			dispatch(checkFlightType());
		}
	};
};

const DateCont = connect(mapStateToProps, mapDispatchToProps)(DateComp);

module.exports = {
	DateCont: DateCont
};