const redux = require('redux');
const types = require('./actionTypes.js');

const PASSENGER_TYPES = {
	adults: 0,
	children: 1,
	infants: 2
};

const AIRPORT_TYPES = {
	origin: 'origin',
	destination: 'destination'
};

const INVALID_SEARCH_PARAMS = {
	none: 'none',
	origin: 'origin',
	destination: 'destination',
	outboundDate: 'outboundDate',
	returnDate: 'returnDate'
};

const initialState = {
	invalidSearchParam: INVALID_SEARCH_PARAMS.none,
	current: {
		origin: '',
		destination: '',
		outboundDate: '',
		returnDate: '',
		passengers: {
			adults: 1,
			children: 0,
			infants: 0
		},
		isRoundTrip: true
	},
	history: []
};

const invalidSearchParam = (state = initialState.invalidSearchParam, action) => {
	switch(action.type){
		case types.INVALID_SEARCH_DONE:
			return action.invalidParam;
		default:
			return state;
	}
};

const history = (state = initialState.history, action) => {
	switch(action.type){
		case types.NEW_SEARCH_ADDED:
			return [...state, action.newSearch];
		default:
			return state;
	}
};

const current = (state = initialState.current, action) => {
	switch(action.type){
		case types.AIRPORT_UPDATED:
			if(action.end === AIRPORT_TYPES.origin) {
				return {...state,  origin: action.airport};
			} else if(action.end === AIRPORT_TYPES.destination) {
				return {...state,  destination: action.airport};
			} else {
				return state;
			}
		case types.OUTBOUND_DATE_UPDATED:
			return {...state, outboundDate: action.date};
		case types.RETURN_DATE_UPDATED:
			return {...state, returnDate: action.date};
		case types.DATES_UPDATED:
			return {...state, outboundDate: action.out, returnDate: action.ret};
		case types.NUMBER_PASSENGERS_UPDATED:
			return {...state, passengers: updateNumberPassengers(state.passengers, action)};
		case types.FLIGHT_TYPE_UPDATED:
			return {...state, isRoundTrip: action.isRoundTrip};
		default:
			return state;
	}
};

const updateNumberPassengers = (state, action) => {
	switch(action.passengerType) {
		case PASSENGER_TYPES.adults:
			return {...state, adults: action.number};
		case PASSENGER_TYPES.children:
			return {...state, children: action.number};
		case PASSENGER_TYPES.infants:
			return {...state, infants: action.number};
		default:
			return state;
	}
};

const reduce = redux.combineReducers({
	invalidSearchParam,
	current,
	history
});

module.exports = {
	initialState: initialState,
	reduce: reduce,
	PASSENGER_TYPES: PASSENGER_TYPES,
	AIRPORT_TYPES: AIRPORT_TYPES,
	INVALID_SEARCH_PARAMS: INVALID_SEARCH_PARAMS
};