const deepFreeze = require('deep-freeze');
const {
	initialState,
	reduce,
	PASSENGER_TYPES,
	AIRPORT_TYPES,
	INVALID_SEARCH_PARAMS
} = require('./reducer.js');
const actions = require('./actions.js');
const redux = require('redux');

describe('reducer', () => {
	it('should update origin', () => {
		const store = redux.createStore(reduce, initialState);
		const action = actions.updateAirport('London', AIRPORT_TYPES.origin);

		deepFreeze(initialState);
		deepFreeze(action);

		const current = {...initialState.current, origin: 'London'};
		const expectedState = {...initialState, current: current};
		store.dispatch(action);
		const actualState = store.getState();

		expect(expectedState).toEqual(actualState);
	});

	it('should update destination', () => {
		const store = redux.createStore(reduce, initialState);
		const action = actions.updateAirport('London', AIRPORT_TYPES.destination);

		deepFreeze(initialState);
		deepFreeze(action);

		const current = {...initialState.current, destination: 'London'};
		const expectedState = {...initialState, current: current};
		store.dispatch(action);
		const actualState = store.getState();

		expect(expectedState).toEqual(actualState);
	});

	it('should update outbund date', () => {
		const store = redux.createStore(reduce, initialState);
		const action = actions.updateOutboundDate('2018-12-12');

		deepFreeze(initialState);
		deepFreeze(action);

		const current = {...initialState.current, outboundDate: '2018-12-12'};
		const expectedState = {...initialState, current: current};
		store.dispatch(action);
		const actualState = store.getState();

		expect(expectedState).toEqual(actualState);
	});

	it('should update return date', () => {
		const store = redux.createStore(reduce, initialState);
		const action = actions.updateReturnDate('2018-12-12');

		deepFreeze(initialState);
		deepFreeze(action);

		const current = {...initialState.current, returnDate: '2018-12-12'};
		const expectedState = {...initialState, current: current};
		store.dispatch(action);
		const actualState = store.getState();

		expect(expectedState).toEqual(actualState);
	});

	it('should update dates', () => {
		const store = redux.createStore(reduce, initialState);
		const action = actions.updateDates('2018-12-12', '2018-12-20');

		deepFreeze(initialState);
		deepFreeze(action);

		const current = {...initialState.current, outboundDate: '2018-12-12', returnDate: '2018-12-20'};
		const expectedState = {...initialState, current: current};
		store.dispatch(action);
		const actualState = store.getState();

		expect(expectedState).toEqual(actualState);

	});

	it('should update number of adult passengers', () => {
		const store = redux.createStore(reduce, initialState);
		const action = actions.updateNumberPassengers(PASSENGER_TYPES.adults, 2)

		deepFreeze(initialState);
		deepFreeze(action);

		const current = {...initialState.current, passengers: {...initialState.current.passengers, adults: 2}}
		const expectedState = {...initialState, current: current};
		store.dispatch(action);
		const actualState = store.getState();

		expect(expectedState).toEqual(actualState);
	});

	it('should update number of child passengers', () => {
		const store = redux.createStore(reduce, initialState);
		const action = actions.updateNumberPassengers(PASSENGER_TYPES.children, 2)

		deepFreeze(initialState);
		deepFreeze(action);

		const current = {...initialState.current, passengers: {...initialState.current.passengers, children: 2}}
		const expectedState = {...initialState, current: current};
		store.dispatch(action);
		const actualState = store.getState();

		expect(expectedState).toEqual(actualState);
	});

	it('should update number of infant passengers', () => {
		const store = redux.createStore(reduce, initialState);
		const action = actions.updateNumberPassengers(PASSENGER_TYPES.infants, 2)

		deepFreeze(initialState);
		deepFreeze(action);

		const current = {...initialState.current, passengers: {...initialState.current.passengers, infants: 2}}
		const expectedState = {...initialState, current: current};
		store.dispatch(action);
		const actualState = store.getState();

		expect(expectedState).toEqual(actualState);
	});

	it('should update type of flight', () => {
		const store = redux.createStore(reduce, initialState);
		const action = actions.updateFlightType(false);

		deepFreeze(initialState);
		deepFreeze(action);

		const current = {...initialState.current, isRoundTrip: false};
		const expectedState = {...initialState, current: current};
		store.dispatch(action);
		const actualState = store.getState();

		expect(expectedState).toEqual(actualState);
	});

	it('should add new search to history', () => {
		const newSearch = {
			origin: 'London',
			destination: 'Edinburgh',
			outboundDate: '2018-12-12',
			returnDate: '2018-12-15',
			passengers: {
				adults: 2,
				children: 1,
				infants: 1
			},
			isRoundTrip: true
		}
		const store = redux.createStore(reduce, initialState);
		const action = actions.addNewSearch(newSearch);

		deepFreeze(initialState);
		deepFreeze(action);

		const history = [...initialState.history, newSearch];
		const expectedState = {...initialState, history: history};
		store.dispatch(action);
		const actualState = store.getState();

		expect(expectedState).toEqual(actualState);
	});

	it('should update invalid search param', () => {
		const newParam = INVALID_SEARCH_PARAMS.origin;
		const store = redux.createStore(reduce, initialState);
		const action = actions.doInvalidSearch(newParam);

		deepFreeze(initialState);
		deepFreeze(action);

		const expectedState = {...initialState, invalidSearchParam: newParam};
		store.dispatch(action);
		const actualState = store.getState();

		expect(expectedState).toEqual(actualState);
	});
});