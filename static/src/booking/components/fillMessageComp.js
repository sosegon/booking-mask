const React = require('react');

const FillMessageComp = () => {
	return(
		<div>
			<span>Please fill out this field.</span>
		</div>
	);
};

module.exports = {
	FillMessageComp: FillMessageComp
};