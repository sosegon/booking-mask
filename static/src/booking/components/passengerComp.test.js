const React = require('react');
const {shallow, mount} = require('enzyme');
const {PassengerComp} = require('./passengerComp.js');

const setup = () => {
	const props = {
		value: 5,
		min: 2,
		max: 7,
		label: 'label test',
		info: 'info test',
		increment: jest.fn(),
		decrement: jest.fn()
	};

	const shallowWrapper = shallow(<PassengerComp {...props} />);
	const mountWrapper = mount(<PassengerComp {...props} />);

	return {
		props,
		shallowWrapper,
		mountWrapper
	};
};

describe('PassengerComp', () => {
	it('should render correctly', () => {
		const {shallowWrapper} = setup();

		expect(shallowWrapper).toMatchSnapshot();
	});

	it('should call prop functions', () => {
		const {mountWrapper, props} = setup();
		const buttons = mountWrapper.find('button');

		buttons.at(0).simulate('click');
		expect(props.decrement).toHaveBeenCalled();

		buttons.at(1).simulate('click');
		expect(props.increment).toHaveBeenCalled();

		mountWrapper.unmount();
	});
});