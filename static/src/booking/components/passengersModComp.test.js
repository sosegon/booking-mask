const React = require('react');
const {shallow, mount} = require('enzyme');
const {Provider} = require('react-redux');
const {PassengersModComp} = require('./passengersModComp.js');
const {PassengerCont} = require('../containers/passengerCont.js');
const {PASSENGER_TYPES, initialState} = require('../reducer.js');
const {storeFake} = require('../../../test/utils.js');

const setup = () => {
	const props = {
	};

	const store = storeFake(initialState);

	const shallowWrapper = shallow(<PassengersModComp {...props} />);
	const mountWrapper = mount(
		<Provider store={store}>
			<PassengersModComp {...props} />
		</Provider>
	);

	return {
		props,
		shallowWrapper,
		mountWrapper
	};
};

describe('PassengersModComp', () => {
	it('should render correctly', () => {
		const {shallowWrapper, mountWrapper} = setup();

		const passengerCont = mountWrapper.find(PassengerCont);
		expect(passengerCont.length).toEqual(3);
		expect(passengerCont.at(0).props().passengerType).toEqual(PASSENGER_TYPES.adults);
		expect(passengerCont.at(1).props().passengerType).toEqual(PASSENGER_TYPES.children);
		expect(passengerCont.at(2).props().passengerType).toEqual(PASSENGER_TYPES.infants);

		expect(shallowWrapper).toMatchSnapshot();
	});
});