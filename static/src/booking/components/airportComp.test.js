const React = require('react');
const {shallow, mount} = require('enzyme');
const {AirportComp} = require('./airportComp.js');

const setup = () => {
	const props = {
		onSuggestionSelected: jest.fn()
	};

	const shallowWrapper = shallow(<AirportComp {...props} />);
	const mountWrapper = mount(<AirportComp {...props} />);

	return {
		props,
		shallowWrapper,
		mountWrapper
	};
};

describe('AirportComp', () => {
	it('should render correctly', () => {
		const {shallowWrapper} = setup();

		const autoSuggestProps = shallowWrapper.find('Autosuggest').props();
		expect(autoSuggestProps.suggestions).toEqual([]);
		expect(autoSuggestProps.onSuggestionsFetchRequested).toBeInstanceOf(Function);
		expect(autoSuggestProps.onSuggestionsClearRequested).toBeInstanceOf(Function);
		expect(autoSuggestProps.getSuggestionValue).toBeInstanceOf(Function);
		expect(autoSuggestProps.renderSuggestion).toBeInstanceOf(Function);
		expect(autoSuggestProps.inputProps).toBeInstanceOf(Object);

		expect(shallowWrapper).toMatchSnapshot();
	});

	it('should update suggestions when text is typed', () => {
		const {mountWrapper} = setup();
		const autoSuggest = mountWrapper.find('Autosuggest');

		let value = 'a';
		autoSuggest.props().onSuggestionsFetchRequested({value});
		expect(mountWrapper.state('suggestions').length).toBeGreaterThan(0);

		value = ''
		autoSuggest.props().onSuggestionsFetchRequested({value});
		expect(mountWrapper.state('suggestions').length).toEqual(0);

		mountWrapper.find('input').simulate('change', {target: {value: 'a'}});
		expect(mountWrapper.state('suggestions').length).toBeGreaterThan(0);

		mountWrapper.unmount();
	});
});