const React = require('react');
const {PassengerCont} = require('../containers/passengerCont.js');
const {PASSENGER_TYPES} = require('../reducer.js');

const {Component} = React;

class PassengersModComp extends Component {

	getSummary = () => {
		const {adults, children, infants} = this.props;

		return adults + " Adults, " + children + " Children, " + infants + " Infants";
	}

	getTotal = () => {
		const {adults, children, infants} = this.props;
		const total = adults + children + infants;

		return total + " Passengers";
	}

	render() {
		const sum = this.getSummary();
		const tot = this.getTotal();
		return(
			<div>
				<div>
					<label>{sum}</label>
					<label>{tot}</label>
				</div>
				<PassengerCont passengerType={PASSENGER_TYPES.adults} info='12+ years'/>
				<PassengerCont passengerType={PASSENGER_TYPES.children} info='2-11 years'/>
				<PassengerCont passengerType={PASSENGER_TYPES.infants} info='0-1 years'/>
			</div>
		);
	}
}

module.exports = {
	PassengersModComp: PassengersModComp
};