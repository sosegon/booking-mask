const React = require('react');
const PropTypes = require('prop-types');

const PassengerComp = ({
	value, min, max, label, info,
	increment,
	decrement
}) => {
	return (
		<div>
			<label>{label}</label>
			<label>{info}</label>
			<button onClick={decrement}>-</button>
			<input
				type='number'
				value={value}
				min={min}
				max={max}
				step='1'
			/>
			<button onClick={increment}>+</button>
		</div>
	);
};

PassengerComp.propTypes = {
	value: PropTypes.number,
	min: PropTypes.number,
	max: PropTypes.number,
	label: PropTypes.string,
	info: PropTypes.string,
	increment: PropTypes.func,
	decrement: PropTypes.func
};

module.exports = {
	PassengerComp: PassengerComp
};