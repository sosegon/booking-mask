const React = require('react');
const Autosuggest = require('react-autosuggest');
const PropTypes = require('prop-types');
const {airportCodes} = require('../airportCodes.js');

const {Component} = React;

const getSuggestions = (value) => {
	const inputValue = value.trim().toLowerCase();
	const inputLength = inputValue.length;

	return inputLength === 0 ? [] : airportCodes.filter(item =>
		item.name.toLowerCase().slice(0, inputLength) === inputValue
	);
};

class AirportComp extends Component {

	constructor() {
		super();
		this.state = {
			suggestions: []
		};
		this.value = '';
	}

	onChange = (event, {newValue}) => {
		this.value = newValue;
	};

	onSuggestionsFetchRequested = ({value}) => {
		this.setState({
			suggestions: getSuggestions(value)
		});
	};

	onSuggestionsClearRequested = () => {
		this.setState({
			suggestions: []
		});
	};

	render() {
		const props = this.props;
		const {suggestions} = this.state;
		const value = this.value;
		const inputProps = {
			placehoder: 'Type a place',
			value,
			onChange: this.onChange
		};

		return(
			<Autosuggest
				suggestions={suggestions}
				onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
				onSuggestionsClearRequested={this.onSuggestionsClearRequested}
				onSuggestionSelected={props.onSuggestionSelected}
				getSuggestionValue={suggestion => {
					return suggestion.name + ' (' + suggestion.code + ')';
				}}
				renderSuggestion={suggestion => (
					<div>
						{suggestion.name} ({suggestion.code})
					</div>
				)}
				inputProps={inputProps}
			/>
		);
	}
}

AirportComp.propTypes = {
	onSuggestionSelected: PropTypes.funct
};

module.exports = {
	AirportComp: AirportComp
};