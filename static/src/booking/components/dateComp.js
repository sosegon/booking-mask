const React = require('react');
const PropTypes = require('prop-types');
const MomentPropTypes = require('react-moment-proptypes');
const {START_DATE, END_DATE} = require('react-dates/constants');
const {DateRangePicker, SingleDatePicker} = require('react-dates');

const {Component} = React;

class DateComp extends Component {

	constructor() {
		super();
		this.state = {
			focusedInput: [START_DATE, END_DATE],
			focused: true
		};
	}

	render() {
		const props = this.props;
		const startDateId = "booking_start_date";
		const endDateId = "booking_end_date";
		let date;
		if(props.isRoundTrip) {
			date = (
				<DateRangePicker
					startDate={props.startDate}
					startDateId={startDateId}
					endDate={props.endDate}
					endDateId={endDateId}
					onDatesChange={props.onDatesChange}
					focusedInput={this.state.focusedInput}
					onFocusChange={focusedInput => this.setState({ focusedInput })}
				/>);
		} else {
			date = (
				<SingleDatePicker
					date={props.startDate}
					onDateChange={props.onOutboundDateChange}
					focused={this.state.focused}
					onFocusChange={({ focused }) => this.setState({ focused })}
				/>
			);
		}
		return (
			<div>
				{date}
				<button onClick={props.onToggleFlights}>
					Return
				</button>
			</div>
		);
	}
}

DateComp.propTypes = {
	startDate: MomentPropTypes.momentObj,
	endDate: MomentPropTypes.momentObj,
	isRoundTrip: PropTypes.bool,
	onDatesChange: PropTypes.func,
	onOutboundDateChange: PropTypes.func,
	onToggleFlights: PropTypes.func
};

module.exports = {
	DateComp: DateComp
};