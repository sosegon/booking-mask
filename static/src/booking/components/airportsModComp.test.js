const React = require('react');
const {shallow, mount} = require('enzyme');
const {Provider} = require('react-redux');
const {AirportsModComp} = require('./airportsModComp.js');
const {AirportCont} = require('../containers/airportCont.js');
const {AIRPORT_TYPES} = require('../reducer.js');
const {storeFake} = require('../../../test/utils.js');

const setup = () => {
	const props = {
	};

	const store = storeFake({});

	const shallowWrapper = shallow(<AirportsModComp {...props} />);
	const mountWrapper = mount(
		<Provider store={store}>
			<AirportsModComp {...props} />
		</Provider>
	);

	return {
		props,
		shallowWrapper,
		mountWrapper
	};
};

describe('AirportsModComp', () => {
	it('should render correctly', () => {
		const {shallowWrapper, mountWrapper} = setup();

		const airportCont = mountWrapper.find(AirportCont);
		expect(airportCont.length).toEqual(2);
		expect(airportCont.at(0).props().end).toEqual(AIRPORT_TYPES.origin);
		expect(airportCont.at(1).props().end).toEqual(AIRPORT_TYPES.destination);

		expect(shallowWrapper).toMatchSnapshot();
	});
});