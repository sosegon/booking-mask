require('react-dates/initialize');
// require('react-dates/lib/css/_datepicker.css');
const moment = require('moment');
const React = require('react');
const {shallow, mount} = require('enzyme');
const {DateComp} = require('./dateComp.js');

const setup = () => {
	const props = {
		startDate: moment('2018/12/10'),
		endDate: moment('2018/12/20'),
		isRoundTrip: true,
		onDatesChange: jest.fn(),
		onOutboundDateChange: jest.fn(),
		onToggleFlights: jest.fn()
	};

	const shallowWrapper = shallow(<DateComp {...props} />);
	const mountWrapper = mount(<DateComp {...props} />);

	return {
		props,
		shallowWrapper,
		mountWrapper
	};
};

describe('DateComp', () => {
	it('should render correctly', () => {
		const {shallowWrapper} = setup();

		expect(shallowWrapper).toMatchSnapshot();
	});

	it('should render the date range picker', () => {
		const {mountWrapper, props} = setup();

		let dateRange = mountWrapper.find('DateRangePicker');
		expect(dateRange.length).toEqual(1);
		let dateSingle = mountWrapper.find('SingleDatePicker');
		expect(dateSingle.length).toEqual(0);

		mountWrapper.unmount();
	});

	it('should render the single range picker', () => {
		const props = {
			startDate: moment('2018/12/10'),
			endDate: moment('2018/12/20'),
			isRoundTrip: false,
			onDatesChange: jest.fn(),
			onOutboundDateChange: jest.fn(),
			onToggleFlights: jest.fn()
		};

		const mountWrapper = mount(<DateComp {...props} />);

		let dateRange = mountWrapper.find('DateRangePicker');
		expect(dateRange.length).toEqual(0);
		let dateSingle = mountWrapper.find('SingleDatePicker');
		expect(dateSingle.length).toEqual(1);

		mountWrapper.unmount();
	});

	it('should call prop functions', () => {
		const {mountWrapper, props} = setup();
		const button = mountWrapper.find('button').at(0);

		button.simulate('click');
		expect(props.onToggleFlights).toHaveBeenCalled();

		mountWrapper.unmount();
	});
});