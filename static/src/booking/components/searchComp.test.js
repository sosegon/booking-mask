const React = require('react');
const {shallow, mount} = require('enzyme');
const {SearchComp} = require('./searchComp.js');

const setup = () => {
	const props = {
		onSearch: jest.fn()
	};

	const shallowWrapper = shallow(<SearchComp {...props} />);
	const mountWrapper = mount(<SearchComp {...props} />);

	return {
		props,
		shallowWrapper,
		mountWrapper
	};
};

describe('SearchComp', () => {
	it('should render correctly', () => {
		const {shallowWrapper} = setup();

		expect(shallowWrapper).toMatchSnapshot();
	});

	it('should call prop functions', () =>  {
		const {mountWrapper, props} = setup();
		const button = mountWrapper.find('button').at(0);

		button.simulate('click');
		expect(props.onSearch).toHaveBeenCalled();

		mountWrapper.unmount();
	});
});