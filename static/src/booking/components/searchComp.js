const React = require('react');
const PropTypes = require('prop-types');

const SearchComp = ({onSearch}) => {
	return(
		<div>
			<button onClick={onSearch}>
				Search
			</button>
		</div>
	);
};

SearchComp.propTypes = {
	params: PropTypes.object,
	onSearch: PropTypes.func
};

module.exports = {
	SearchComp: SearchComp
};