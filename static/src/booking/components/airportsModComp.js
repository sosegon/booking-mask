const React = require('react');
const {AirportCont} = require('../containers/airportCont.js');
const {AIRPORT_TYPES} = require('../reducer.js');

const {Component} = React;

class AirportsModComp extends Component {
	render() {
		return (
			<div>
				<AirportCont end={AIRPORT_TYPES.origin} />
				<AirportCont end={AIRPORT_TYPES.destination} />
			</div>
		);
	}
}

module.exports = {
	AirportsModComp: AirportsModComp
};