const React = require('react');
const {shallow} = require('enzyme');
const {FillMessageComp} = require('./fillMessageComp.js');

const setup = () => {
	const shallowWrapper = shallow(<FillMessageComp />);
	return {
		shallowWrapper
	};
};

describe('FillMessageComp', () => {
	it('should render correctly', () => {
		const {shallowWrapper} = setup();

		expect(shallowWrapper).toMatchSnapshot();
	});
});