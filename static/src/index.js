require('react-dates/initialize');
require('react-dates/lib/css/_datepicker.css');
const React = require('react');
const ReactDOM = require('react-dom');
const {createStore, applyMiddleware} = require('redux');
const thunk = require('redux-thunk').default;
const {Provider} = require('react-redux');
const {AirportsModCont} = require('./booking/containers/airportsModCont.js');
const {DateCont} = require('./booking/containers/dateCont.js');
const {SearchCont} = require('./booking/containers/searchCont.js');
const {PassengersModCont} = require('./booking/containers/passengersModCont.js');
const {initialState, reduce, AIRPORT_TYPES, PASSENGER_TYPES} = require('./booking/reducer.js');

const BookingApp = () => (
	<div>
		<PassengersModCont />
		<AirportsModCont />
		<DateCont />
		<SearchCont />
	</div>
);

const store = createStore(reduce, initialState, applyMiddleware(thunk));

ReactDOM.render(
	<Provider store={store}>
		<BookingApp/>
	</Provider>,
	document.getElementById('root')
);
