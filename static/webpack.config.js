var path = require('path');
var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
	entry: {
		index: path.join(__dirname, './src/index.js')
	},
	output: {
		filename: 'build.js',
		path: path.join(__dirname, '/dist/js/')
	},
	module: {
		rules: [
			{
				test: /\.html$/,
				use: [{loader: 'html-loader', options: {minimize:false}}]
			},
			{
				test: /\.js$/,
				loader: 'babel-loader'
			},
			{
				test: /\.s?css$/,
				use: [
					'style-loader',
					'css-loader',
					'sass-loader'
				]
			}
		]
	},
	plugins: [
		new HtmlWebpackPlugin({
			title: 'Booking Mask',
			filename: '../index.html',
			template: './src/index.html'
		}),
		new webpack.ProvidePlugin({
			'React' : 'react'
		})
	]
};
